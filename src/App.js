import React, {Component} from 'react';
import {Button,ButtonGroup, Row, Col, Input, Form, ListGroup, ListGroupItem} from 'reactstrap';

class App extends Component {
    state = {
        todos: [],
        value: '',
        id: 1,
        status:'all'
    }

    render() {
        const {value, todos,status} = this.state
        const onSubmit = (e) => {
            e.preventDefault();
            let value = this.state.value;
            if (value) {
                let todos = this.state.todos;
                todos.push({
                    id: this.state.id,
                    title: value,
                    completed: false
                });
                this.setState({
                    id:this.state.id+1,
                    todos: todos,
                    value: ''
                })
            }
        }
        const onChange = (e) => {
            this.setState({
                value: e.target.value
            })
        }
        const changecompleted = (id) =>{
            this.setState({
                todos:this.state.todos.map(i=>{
                    return i.id===id?{id:i.id,title: i.title,completed: !i.completed}:i
                })
            })

        }
        const nonCompletedStyle = {
            cursor: 'pointer',
            width:'90%',
            display: 'inline',
            overflow:'hidden'
        }
        const completedStyle = {
            ...nonCompletedStyle,
            color :'red'
        }
        const deleteTodo =(id)=>{
            this.setState({
                todos:todos.filter(i=>i.id!==id)
            })
        }
        const changeStatus =(status)=>{
            this.setState({status})
        }

        return (

            <Row>
                <Col sm="12" md={{size: 3, offset: 4}}>
                    <Form onSubmit={onSubmit}>
                        <Input onChange={onChange} value={value} name="title"
                               placeholder="What needs to be done ? " required/>
                    </Form>
                    <ListGroup>
                        {todos
                            .filter(i=>status==='all'?i:status==='active'?(!i.completed):i.completed)
                            .map((item, index) => {
                            return <div style={{display:'flex',minWidth:'100%'}}> <ListGroupItem style={item.completed?completedStyle:nonCompletedStyle} key={index} onClick={()=>changecompleted(item.id)} color="info" className="justify-content-between">{item.title}
                            </ListGroupItem><Button onClick={()=>deleteTodo(item.id)} style={{display:'inline',color: 'info'}} color="danger" size="lg">x</Button>
                            </div>
                        })}
                    </ListGroup>
                    <ButtonGroup style={{width: '100%'}}>
                        <Button onClick={()=>changeStatus('all')}>All</Button>
                        <Button onClick={()=>changeStatus('active')}>Active</Button>
                        <Button onClick={()=>changeStatus('completed')}>Completed</Button>
                    </ButtonGroup>
                </Col>
            </Row>
        )
    }

}

export default App;
